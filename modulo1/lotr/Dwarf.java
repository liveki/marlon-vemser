
public class Dwarf
{
    private String nome;
    private double vida;
    private Status status;
    
    {
        vida = 110.00;
        status = Status.RECEM_CRIADO;
    }
    
    public Dwarf(String nome) {
        this.nome = nome;
    }
    
    public String getNome() {
       return this.nome;
   }
   
   public void setNome(String nome) {
       this.nome = nome;
   }
   
   public void sofrerDano() {
       if(status != Status.MORTO) {
           this.vida -= 10.00;
       
           if(vida == 0) {
               status = Status.MORTO;
           }
       }
   }
   
   public double getVida() {
       return this.vida;
   }
}
