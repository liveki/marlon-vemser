import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest {
    @Test
    public void dwarfDeveNascerCom110Vidas() {
        Dwarf dwarfQualquer = new Dwarf("Anao");
        assertEquals(110.00, dwarfQualquer.getVida(), 0);
    }
    
    @Test
    public void dwarfDevePerderVida() {
        Dwarf dwarfQualquer = new Dwarf("Anao");
        dwarfQualquer.sofrerDano();
        
        assertEquals(100.00, dwarfQualquer.getVida(), 0);
    }
    
    @Test
    public void dwarfZeraVida() {
        Dwarf dwarf = new Dwarf("Anao");
        
        for(int i = 0; i < 12; i++) {
            dwarf.sofrerDano();
        }
        
        assertEquals(0.0, dwarf.getVida(), .01);
    }
    
    @Test
    public void naoPodeSofrerDanoZeroVidas() {
        Dwarf dwarf = new Dwarf("Anao");
        
        for(int i = 0; i < 13; i++) {
            dwarf.sofrerDano();
        }
        
        assertEquals(0.0, dwarf.getVida(), .01);
    }
}
