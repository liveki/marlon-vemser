public class Elfo { 
   private String nome;
   private Inventario inventario;
   private int indiceFlecha;
   private int experiencia;
   private Status status;
   
   {
       this.inventario = new Inventario();
       this.indiceFlecha = 1;
       this.experiencia = 0;
       this.status = Status.RECEM_CRIADO;
   }
   
   public Elfo(String nome) {
       this.nome = nome;
       inventario.adicionar(new Item(1, "Arco"));
       inventario.adicionar(new Item(2, "Flecha"));
   }
   
   public String getNome() {
       return this.nome;
   }
   
   public void setNome(String nome) {
       this.nome = nome;
   }
   
   public int getExperiencia() {
       return this.experiencia;
   }
   
   private void aumentarXP() {
       experiencia++;
   }
   
   public Item getItem(int index) {
       return this.inventario.obter(index);
   }
   
   public Item getFlecha() {
       return this.inventario.obter(indiceFlecha);
   }
   
   public Inventario getInventario() {
       return this.inventario;
    }
   
   public Object atirarFlecha(Object dwarf) {
       if(dwarf instanceof Dwarf) {
           int qtdAtual = this.getFlecha().getQuantidade();
           
           if(this.podeAtirarFlecha()) {
               this.getFlecha().setQuantidade(qtdAtual - 1);
               ((Dwarf)dwarf).sofrerDano();
               this.aumentarXP();
           }
       }
       
       return dwarf;
   }
    
    public boolean podeAtirarFlecha() {
        return this.getFlecha().getQuantidade() > 0;
    }
}