import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest {
    
    @Test
    public void elfoDeveNascerCom2Flechas() {
        Elfo elfoQualquer = new Elfo("Legolas");
        
        assertEquals(2, elfoQualquer.getFlecha().getQuantidade());
    }
    
    @Test
    public void atirarFlechaDevePerderFlechaAumentarXP() {
        Dwarf dwarfQualquer = new Dwarf("Anao");
        Elfo elfoQualquer = new Elfo("Legolas");
        
        elfoQualquer.atirarFlecha(dwarfQualquer);
        
        assertEquals(1, elfoQualquer.getFlecha().getQuantidade());
        assertEquals(1, elfoQualquer.getExperiencia());
        assertEquals(100.00, dwarfQualquer.getVida(), 0);
    }
    
    @Test
    public void atirar3FlechasNaoDeveSerPossivel() {
        Dwarf dwarfQualquer = new Dwarf("Anao");
        Elfo elfoQualquer = new Elfo("Legolas");
        
        elfoQualquer.atirarFlecha(dwarfQualquer);
        elfoQualquer.atirarFlecha(dwarfQualquer);
        
        assertEquals(0, elfoQualquer.getFlecha().getQuantidade());
        
        elfoQualquer.atirarFlecha(dwarfQualquer);
        
        assertEquals(0, elfoQualquer.getFlecha().getQuantidade());
    }
    
    @Test
    public void naoDeveAtirarFlechasQualquerObjeto() {
        Dwarf dwarfQualquer = new Dwarf("Anao");
        Elfo elfoQualquer = new Elfo("Legolas");
        elfoQualquer.atirarFlecha(new Elfo("Pelegolas"));
        
        assertEquals(2, elfoQualquer.getFlecha().getQuantidade());
        assertEquals(110.00, dwarfQualquer.getVida(), 0);
    }
}
