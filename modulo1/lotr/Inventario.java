public class Inventario {
    int quantidade;
    Item[] itens;
    
    public Inventario (int qtd) {
        itens = new Item[qtd];
        this.quantidade = 0;
    }
    
    public Inventario () {
        this(99);
    }
    
    public Item[] getItens() {
        return this.itens;
    }
    
    public int getQuantidade() {
        return this.quantidade;
    }
    
    public void adicionar(Item item) {
        for(int i = 0; i < itens.length; i++) {
            if(itens[i] == null) {
                itens[i] = item;
                quantidade++;
                break;
            }
        }
    }
    
    public Item obter(int index) {
        if(!indiceValido(index)) {
            return null;
        }
        
        return itens[index];
    }
    
    public Item obterPorNome(String nome) {
        for(Item item : itens) {
            if(item.getDescricao().equals(nome)) {
                return item;
            }
        }
        
        return null;
    }
    
    public void remover(int index) {
        if(!indiceValido(index)) {
            return;
        }
        
        itens[index] = null;
        quantidade--;
    }
    
        
    public boolean indiceValido(int index) {
        return index >= 0 && index < itens.length;
    }
    
    
    public Item getItemMaiorQuantidade() {
        int maiorQtd = 0;
        Item item = null;
        
        for(int i = 0; i < this.itens.length; i++) {
            if(itens[i] != null) {
                if(itens[i].getQuantidade() > maiorQtd) {
                    maiorQtd = itens[i].getQuantidade();
                    item = itens[i];
                }
            }
        }
        
        return item;
    }
    
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        
        for(int i = 0; i < itens.length; i++) {
            if(itens[i] != null) {
                if(i == 0) {
                    sb.append(itens[i].getDescricao());
                } else {
                    sb.append(",");
                    sb.append(itens[i].getDescricao());
                }
            }
        }
        
        return sb.toString();
    }
}
