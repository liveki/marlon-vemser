import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest {
    @Test
    public void deveCriarInventario99Slots() {
        Inventario inventario = new Inventario();
        
        assertEquals(99, inventario.getItens().length);
    }
    
    @Test
    public void deveCriarInventario3Slots() {
        Inventario inventario = new Inventario(3);
        assertEquals(3, inventario.getItens().length);
    }
    
    @Test
    public void adicionarUmItem() {
        Inventario inventario = new Inventario();
        Item item = new Item(5, "Flechas");
        inventario.adicionar(item);
        assertEquals(item, inventario.getItens()[0]);
    }
    
    @Test
    public void adicionarDoisItens() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(1,"Arco"));
        inventario.adicionar(new Item(5,"Flechas"));
        assertEquals(2, inventario.getQuantidade());
    }
    
    @Test
    public void obterItem() {
        Inventario inventario = new Inventario();
        Item item = new Item(1,"Arco");
        inventario.adicionar(item);
        assertEquals(item, inventario.obter(0));
    }
    
    @Test
    public void deveRemoverItem() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(5, "Flechas"));
        inventario.remover(0);
        assertNull(inventario.obter(0));
    }
    
    @Test
    public void naoDeveObterItemPosicaoInvalida() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(5, "Flechas"));
        
        assertNull(inventario.obter(1));
    }
    
    @Test
    public void deveRetornarNomeItens() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(1, "Adaga"));
        inventario.adicionar(new Item(1, "Escudo"));
        inventario.adicionar(new Item(1, "Bracelete"));
        
        String esperado = "Adaga,Escudo,Bracelete";
        
        assertEquals(esperado, inventario.toString());
    }
    
    @Test
    public void deveDevolverItemMaiorQuantidade() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(5, "Adaga"));
        inventario.adicionar(new Item(9, "Escudo"));
        inventario.adicionar(new Item(3, "Bracelete"));
        
        Item obtido = new Item(9, "Escudo");
        
        assertEquals(obtido, inventario.getItemMaiorQuantidade());
    }
}
