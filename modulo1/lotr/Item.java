public class Item {
    //quantidade, descricao
    private String descricao;
    private int quantidade;
    
    public Item(int quantidade, String descricao) {
        this.descricao = descricao;
        this.quantidade = quantidade;
    }
    
    public String getDescricao() {
        return this.descricao;
    }
    
    
    public int getQuantidade() {
        return this.quantidade;
    }
    
    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    @Override
    public boolean equals(Object item) {
        return this.descricao == ((Item)item).descricao;
    }
}
